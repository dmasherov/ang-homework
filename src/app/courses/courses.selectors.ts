import { createFeatureSelector, createSelector } from '@ngrx/store';
import { pipe } from 'rxjs';
import { filter } from 'rxjs/operators';

import { CoursesState } from './courses.reducer';

export const selectCoursesState = createFeatureSelector('courses');

export const selectCourses = createSelector(
  selectCoursesState,
  (state: CoursesState) => state
);

export const selectItems = createSelector(
  selectCoursesState,
  (state: CoursesState) => state.items
);

export const selectCanLoadMore = createSelector(
  selectCoursesState,
  (state: CoursesState) => state.canLoadMore
);

export const selectCurrentItem = createSelector(
  selectCoursesState,
  (state: CoursesState) => state.currentItem
);

export const selectSuggestedAuthors = createSelector(
  selectCoursesState,
  (state: CoursesState) => state.suggestedAuthors
);
