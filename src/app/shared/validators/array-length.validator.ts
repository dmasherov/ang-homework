import { AbstractControl, ValidationErrors } from '@angular/forms';

export function minLengthValidator(min: number) {
  return (control: AbstractControl): ValidationErrors => {
    const val = control.value;
    const arrLength = control.value.length;

    const errors: any = {};
    if (arrLength < min) {
      return { minLength: true };
    }
    return null;
  };
}

export function maxLengthValidator(max: number) {
  return(control: AbstractControl): ValidationErrors => {
    const val = control.value;
    const arrLength = control.value.length;

    const errors: any = {};
    if (arrLength > max) {
      return { maxLength: true };
    }
    return null;
  };
}
