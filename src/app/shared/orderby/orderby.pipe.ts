import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderby'
})
export class OrderByPipe implements PipeTransform {

  public transform<T>(array: T[], order: 'desc' | 'asc' = 'desc', field?: keyof T): T[] {
    if (!array) { return []; }
    const orderDir = order === 'desc' ? 1 : -1;
    const sorted = array.slice().sort((aObj, bObj) => {
      const a = field ? aObj[field] : aObj;
      const b = field ? bObj[field] : bObj;
      if (a < b) {
        return -1 * orderDir;
      }
      if (a > b) {
        return 1 * orderDir;
      }
      return 0;
    });
    return sorted;
  }

}
