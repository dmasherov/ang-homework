import { OrderByPipe } from './orderby.pipe';

describe('OrderByPipe', () => {
  const pipe = new OrderByPipe();

  it('can sort and desc is default order', () => {
    const arr = [1, 3, 2];
    const sorted = pipe.transform(arr);
    expect(sorted).toEqual([1, 2, 3]);
  });

  it('can sort by asc', () => {
    const arr = [1, 3, 2];
    const sorted = pipe.transform(arr, 'asc');
    expect(sorted).toEqual([3, 2, 1]);
  });

  it('can sort by keys in object', () => {
    interface T {
      id: number;
      s: string;
    }
    const arr: T[] = [ {id: 1, s: '1'}, {id: 3, s: '3'}, {id: 2, s: '2'} ];
    const sorted = pipe.transform(arr, 'desc', 's');
    const expected = [arr[0], arr[2], arr[1]];
    expect(sorted).toEqual(expected);
  });
});
