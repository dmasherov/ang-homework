import { createAction, props } from '@ngrx/store';

import { Course, Person } from './model';

export const initialize = createAction('[Courses] Initialize');
export const loadMore = createAction('[Courses] Load More');
export const loadSuccess = createAction('[Courses] Load Success', props<{items: Course[]}>());
export const loadFailure = createAction('[Courses] Load Failure');
export const search = createAction('[Courses] Search', props<{searchTerm: string}>());

export const deleteItem = createAction('[Courses] Delete', props<{id: number}>());
export const addEditItem = createAction('[Courses] Add/Edit', props<{itemId?: number}>());
export const itemLoaded = createAction('[Courses] Item loaded', props<{item: Course}>());
export const cancelEdit = createAction('[Courses] Cancel Edit');
export const updateItem = createAction('[Courses] Update', props<{id: number, item: Course}>());
export const createItem = createAction('[Courses] Create', props<{item: Course}>());

export const searchAuthors = createAction('[Courses] Search Authors', props<{searchTerm: string}>());
export const searchAuthorsResult = createAction('[Courses] Search Authors Result', props<{personList: Person[]}>());

export const navigateToList = createAction('[Courses] Navigate to list');
