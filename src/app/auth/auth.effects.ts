import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import * as AuthActions from './auth.actions';
import { AuthService } from './auth.service';

@Injectable()
export class AuthEffects {

  public login$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.login),
    switchMap(({email, password}) => {
      return this.authService.login(email, password).pipe(
        map(user => {
          this.router.navigate(['/courses']);
          return AuthActions.loginDone({user});
        }),
        catchError(err => {
          let message;
          if (err.error && err.error.message) {
            if (err.error.status == 401) {
              message = 'Wrong e-mail or password';
            }
            else {
              message = err.error.message;
            }
          } else {
            message = 'Service Login error occured';
          }
          return of(AuthActions.loginFail({message}));
        })
      );
    })
  ));

  public userFromLocalStorage$ = createEffect(() => this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    map(action => {
      const userFromStorage = this.authService.getUserInfoInStorage();
      return AuthActions.loginDone({user: userFromStorage});
    })
  ));

  public logout$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.logout),
    map(() => {
      this.authService.logout();
      return this.router.navigate(['/login']);
    }),
    map(() => AuthActions.logoutDone())
  ));

  constructor(private actions$: Actions, private authService: AuthService, private router: Router) {}
}
