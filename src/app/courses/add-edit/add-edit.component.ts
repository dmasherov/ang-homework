import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { integerValidator, minLengthValidator } from '../../shared';
import * as CourseActions from '../courses.actions';
import { CoursesState } from '../courses.reducer';
import { selectCurrentItem, selectSuggestedAuthors } from '../courses.selectors';
import { Person } from '../model';

@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.css']
})
export class AddEditComponent implements OnDestroy {

  private readonly LOCAL_STORAGE_KEY = 'addEditForm';
  private currentItemSubscription: Subscription;
  private addEditFormChangeSub: Subscription;

  public courseId: number;
  public ready = false;
  public addEditForm: FormGroup;
  public authorsSuggestions: Person[] = [];

  get title() { return this.addEditForm.get('title'); }
  get description() { return this.addEditForm.get('description'); }
  get creationDate() { return this.addEditForm.get('creationDate'); }
  get duration() { return this.addEditForm.get('duration'); }
  get authors() { return this.addEditForm.get('authors'); }

  public obj = { 'a': 1 }
  public key = 'a'
  constructor(private fb: FormBuilder, private store: Store<CoursesState>, route: ActivatedRoute) {
    const courseIdParam = route.snapshot.params.id;
    this.store.select(selectSuggestedAuthors).subscribe(personList => {
      this.authorsSuggestions = personList.slice(0, 5);
    });

    this.addEditForm = fb.group({
      title: ['', [Validators.required, Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.maxLength(500)]],
      creationDate: ['', Validators.required],
      duration: ['', [Validators.required, integerValidator, Validators.min(1)]],
      authors: [[], [minLengthValidator(1)]],
    });

    if (courseIdParam) {
      this.courseId = parseInt(courseIdParam, 10);
      this.store.dispatch(CourseActions.addEditItem({itemId: this.courseId}));
      this.currentItemSubscription = this.store.select(selectCurrentItem).subscribe(course => {
        if (!course) {
          return;
        }
        this.addEditForm.patchValue(course);
        this.ready = true;
      });
    } else {
      this.ready = true;
      const localData = this.readFromLocalStorage();
      this.addEditForm.patchValue(localData);
      const addEditFormChangeSub = this.addEditForm.valueChanges.subscribe(val => {
        this.writeToLocalStorage(val);
      });
    }
  }

  public searchAuthors(searchTerm: string) {
    this.store.dispatch(CourseActions.searchAuthors({searchTerm}));
  }

  public save(): void {
    if (this.addEditForm.invalid) {
      return;
    }
    const course = this.addEditForm.value;
    if (this.courseId) {
      this.store.dispatch(CourseActions.updateItem({id: this.courseId, item: course}));
    } else {
      this.store.dispatch(CourseActions.createItem({item: course}));
    }
  }

  public cancel(): void {
    this.store.dispatch(CourseActions.cancelEdit());
  }

  public ngOnDestroy() {
    if (this.currentItemSubscription) {
      this.currentItemSubscription.unsubscribe();
    }
    if (this.addEditFormChangeSub) {
      this.addEditFormChangeSub.unsubscribe();
    }
  }

  private readFromLocalStorage(): any {
    try {
      const localDataStr = localStorage.getItem(this.LOCAL_STORAGE_KEY);
      console.log(localDataStr);
      const localData = JSON.parse(localDataStr);
      return localData ? localData : {};
    } catch (e) {
      return {};
    }
  }

  private writeToLocalStorage(localData): void {
    const localDataStr = JSON.stringify(localData);
    localStorage.setItem(this.LOCAL_STORAGE_KEY, localDataStr);
  }

}
