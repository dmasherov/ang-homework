import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  it('should ', () => {
    const authService = new AuthService();

    const username = 'username';
    const token = 'token';

    spyOn(localStorage, 'setItem');
    spyOn(localStorage, 'getItem').and.returnValue(username);

    authService.login(username, token);
    expect(localStorage.setItem).toHaveBeenCalledWith('user', username);

    expect(authService.getUserInfo()).toBe(username);
  });
});
