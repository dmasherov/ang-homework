import { TestBed } from '@angular/core/testing';

import { CourseService } from './course.service';
import { Course } from './model';

describe('CourseService', () => {
  let courseService: CourseService;

  beforeEach(() => {
    courseService = new CourseService();
  });

  const createMockCourse = (id: number) => ({
    id,
    title: 'test course',
    creationDate: '2019-11-12',
    duration: 120,
    description: 'desc',
    topRated: false
  });

  it('should be able to create a course and get it by id', () => {
    const courseCount = courseService.getCourses().length;
    courseService.createCourse(createMockCourse(100));
    const newCourseCount = courseService.getCourses().length;
    expect(newCourseCount).toEqual(courseCount + 1);
  });

  it('should be able to create a course and get it by id', () => {
    const mockCourse = createMockCourse(100);
    courseService.createCourse(mockCourse);
    const newCourse = courseService.getCourseById(mockCourse.id);
    expect(newCourse).toEqual(mockCourse);
  });

  it('should allow to remove a course by id', () => {
    const mockCourse = createMockCourse(100);
    courseService.createCourse(mockCourse);
    expect(courseService.getCourseById(mockCourse.id)).toBeDefined();
    courseService.removeCourse(mockCourse.id);
    expect(courseService.getCourseById(mockCourse.id)).not.toBeDefined();
  });
});
