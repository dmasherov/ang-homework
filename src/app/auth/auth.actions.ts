import { createAction, props } from '@ngrx/store';

import { User } from './user';

export const login = createAction('[Auth] Login', props<{email: string, password: string}>());
export const loginDone = createAction('[Auth] Login Done', props<{user: User}>());
export const loginFail = createAction('[Auth] Login Fail', props<{message: string}>());
export const logout = createAction('[Auth] Logout');
export const logoutDone = createAction('[Auth] Logout Done');
