import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';
import { CloudTagsInputComponent } from './cloud-tags-input/cloud-tags-input.component';
import { DateInputComponent } from './date-input/date-input.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { LoadingBlockComponent } from './loading-overlay/loading-block.component';
import { LogoComponent } from './logo/logo.component';
import { OrderByPipe } from './orderby/orderby.pipe';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SearchInputComponent } from './search-input/search-input.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    LogoComponent,
    PageNotFoundComponent,
    LoadingBlockComponent,
    OrderByPipe,
    DateInputComponent,
    CloudTagsInputComponent,
    SearchInputComponent,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    LogoComponent,
    PageNotFoundComponent,
    LoadingBlockComponent,
    OrderByPipe,
    DateInputComponent,
    CloudTagsInputComponent,
    SearchInputComponent,
    TranslateModule,
  ],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
  ]
})
export class SharedModule { }
