import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import * as Auth from '../../auth';
import { User } from '../../auth';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnDestroy {

  private userSubscription: Subscription;

  public user: User;
  public currentLang: string;

  constructor(private translateService: TranslateService, private store: Store<Auth.AuthState>, private router: Router) {
    this.store.select(Auth.selectUser).subscribe(user => {
      this.user = user;
    });
    this.currentLang = this.translateService.currentLang;
  }

  public logoff() {
    this.store.dispatch(Auth.logout());
  }

  public ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  public changeLanguage(lang: string) {
    this.translateService.use(lang);
  }
}
