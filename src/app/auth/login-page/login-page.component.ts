import { Component, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import * as AuthActions from '../auth.actions';
import { AuthState } from '../auth.reducer';
import { selectErrorMessage } from '../auth.selectors';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnDestroy {

  @ViewChild('loginForm', { static: true })
  public loginForm: NgForm;

  public email: string;
  public password: string;
  public error: string;

  private errorSubscription: Subscription;

  constructor(private store: Store<AuthState>, private router: Router) {
    this.errorSubscription = this.store.select(selectErrorMessage).subscribe(message => {
      this.error = message;
    });
  }

  public login(): void {
    if (this.loginForm.invalid) {
      return;
    }
    this.store.dispatch(AuthActions.login({ email: this.email, password: this.password}));
  }

  public ngOnDestroy() {
    this.errorSubscription.unsubscribe();
  }
}
