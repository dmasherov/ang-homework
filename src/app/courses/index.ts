export { AddEditComponent } from './add-edit/add-edit.component';
export { CourseListComponent } from './list/course-list.component';
export { CourseComponent } from './course/course.component';
export { CoursesModule } from './courses.module';
