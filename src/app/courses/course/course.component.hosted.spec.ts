import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { Course } from '../model';
import { HourMinuteFormatPipe } from '../utils/hour-minute-format.pipe';

import { CourseComponent } from './course.component';

describe('Hosted CourseComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseComponent, TestHostComponent, HourMinuteFormatPipe ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should show course id and uppercased title in title element', () => {
    const titleDe = fixture.debugElement.query(By.css('.header > .title'));
    const titleEl = titleDe.nativeElement;
    fixture.detectChanges();

    const { id: expectedId, title: expectedTitle } = component.course;
    expect(titleEl.textContent).toContain(`Video Course ${expectedId}. ${expectedTitle.toUpperCase()}`);
  });
});

@Component({
  template: `
    <app-course [course]="course" (onDelete)="onSelected($event)">
    </app-course>`
})
class TestHostComponent {
  public course: Course = {
    id: 1,
    title: 'title',
    creationDate: '2019-01-01',
    duration: 20,
    description: 'description',
    topRated: false,
    authors: [],
  };
  public selectedCourse: Course;
  public onSelected(course: Course) { this.selectedCourse = course; }
}

