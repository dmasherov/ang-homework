import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from './user';
const LOGIN_URL = '/api/auth/login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private userInfoSource: BehaviorSubject<User>;
  public userInfo$: Observable<User>;

  constructor(private http: HttpClient) {
    this.userInfoSource = new BehaviorSubject(this.getUserInfoInStorage());
    this.userInfo$ = this.userInfoSource.asObservable();
  }

  public login(email: string, password: string): Observable<User> {
    return this.http.post<{token: string}>(LOGIN_URL, { email, password }).pipe(
      map(jwt => {
        localStorage.setItem('email', email);
        localStorage.setItem('token', jwt.token);
        const user: User = { name: email, email, token: jwt.token };
        this.userInfoSource.next(user);
        return user;
      })
    );
  }

  public logout(): void {
    localStorage.removeItem('email');
    localStorage.removeItem('token');
    this.userInfoSource.next(null);
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  public getUserInfoInStorage(): User {
    const email = localStorage.getItem('email');
    const token = localStorage.getItem('token');
    if (email && token) {
      return {
        name: localStorage.getItem('email'),
        email: localStorage.getItem('email'),
        token: localStorage.getItem('token')
      };
    } else {
      return null;
    }
  }

}
