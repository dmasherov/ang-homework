import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { from, of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { CourseService } from './course.service';
import * as CourseActions from './courses.actions';
import { CoursesState } from './courses.reducer';

@Injectable()
export class CoursesEffects {
  public loadCourses$ = createEffect(() => this.actions$.pipe(
    ofType(CourseActions.initialize, CourseActions.loadMore, CourseActions.search),
    withLatestFrom(this.store$),
    switchMap(([_, state]) => {
      const { currentPage, searchTerm } = state.courses;
      return this.courseService.getCoursePage(currentPage, searchTerm).pipe(
        map(items => CourseActions.loadSuccess({ items })),
        catchError(error => of(CourseActions.loadFailure()))
      );
    })
  ));

  public loadAddEditItem$ = createEffect(() => this.actions$.pipe(
    ofType(CourseActions.addEditItem),
    switchMap(({ itemId }) => {
      return this.courseService.getCourseById(itemId).pipe(
        map(item => CourseActions.itemLoaded({ item })),
        catchError(error => of(CourseActions.loadFailure()))
      );
    })
  ));

  public deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(CourseActions.deleteItem),
    switchMap(({ id }) => {
      return this.courseService.removeCourse(id).pipe(
        map(items => CourseActions.initialize()),
        catchError(error => of(CourseActions.loadFailure()))
      );
    })
  ));

  public updateItem$ = createEffect(() => this.actions$.pipe(
    ofType(CourseActions.updateItem),
    switchMap(({ id, item }) => {
      return this.courseService.updateCourse(id, item).pipe(
        map(items => CourseActions.navigateToList()),
        catchError(error => of(CourseActions.loadFailure()))
      );
    })
  ));

  public createItem$ = createEffect(() => this.actions$.pipe(
    ofType(CourseActions.createItem),
    switchMap(({ item }) => {
      return this.courseService.createCourse(item).pipe(
        map(items => CourseActions.navigateToList()),
        catchError(error => of(CourseActions.loadFailure()))
      );
    })
  ));

  public searchAuthors = createEffect(() => this.actions$.pipe(
    ofType(CourseActions.searchAuthors),
    switchMap(({searchTerm}) => {
      return this.courseService.searchAuthors(searchTerm).pipe(
        map(personList => CourseActions.searchAuthorsResult({personList})),
        catchError(error => of(CourseActions.loadFailure()))
      );
    })
  ));

  public navigationToCourses$ = createEffect(() => this.actions$.pipe(
    ofType(CourseActions.navigateToList, CourseActions.cancelEdit),
    switchMap(() => from(this.router.navigate(['courses'])).pipe(
      map(() => CourseActions.initialize())
    ))
  ));

  constructor(private actions$: Actions, private store$: Store<any>, private courseService: CourseService, private router: Router) {}
}
