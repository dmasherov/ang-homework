import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { zip } from 'rxjs';

@Pipe({
  name: 'hourMinuteFormat',
  pure: false,
})
export class HourMinuteFormatPipe implements PipeTransform {

  private message = '';

  constructor(private translateService: TranslateService) {
  }

  public transform(durationMin: number): string {
    const hourMinTranslate = zip(
      this.translateService.get('time.short-hour'),
      this.translateService.get('time.short-minute')
    ).subscribe(([hourTr, minTr]) => {
      if (!durationMin) {
        this.message = '';
      }
      if (durationMin >= 60) {
        this.message =  `${Math.floor(durationMin / 60)}${hourTr} ${durationMin % 60}${minTr}`;
      }
      else {
        this.message = `${durationMin % 60}${minTr}`;
      }
    })
    return this.message;
  }

}
