import { Component, OnDestroy, OnInit } from '@angular/core';
import { State, Store} from '@ngrx/store';
import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';

import * as CourseActions from '../courses.actions';
import { CoursesState } from '../courses.reducer';
import { selectCanLoadMore, selectItems } from '../courses.selectors';
import { Course } from '../model';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit, OnDestroy {

  constructor(private store: Store<CoursesState>) {
  }

  private searchTerms = new Subject<string>();

  public canLoadMore = false;

  private searchTermSubscription: Subscription;

  public coursesItems$: Observable<Course[]>;
  public canLoadMore$: Observable<boolean>;

  public search(term: string): void {
    this.searchTerms.next(term);
  }

  public loadMore(): void {
    this.store.dispatch(CourseActions.loadMore());
  }

  public delete(id: number): void {
    const confirmed = window.confirm('Do you really want to delete this course?');
    if (confirmed) {
      this.store.dispatch(CourseActions.deleteItem({id}));
    }
  }

  public ngOnInit() {
    this.searchTermSubscription = this.searchTerms.pipe(
      filter(text => text === '' || text.length >= 3),
      debounceTime(300),
      distinctUntilChanged(),
    ).subscribe(searchTerm => {
      this.store.dispatch(CourseActions.search({searchTerm}));
    });

    this.coursesItems$ = this.store.select(selectItems);
    this.canLoadMore$ = this.store.select(selectCanLoadMore);
    this.store.dispatch(CourseActions.initialize());
  }

  public ngOnDestroy() {
    this.searchTermSubscription.unsubscribe();
  }
}

