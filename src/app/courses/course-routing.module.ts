import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../auth';

import { AddEditComponent } from './add-edit/add-edit.component';
import { CourseListComponent } from './list/course-list.component';

const routes: Routes = [
  {
    path: 'courses',
    component: CourseListComponent,
    canActivate: [ AuthGuard ],
  },
  {
    path: 'courses/new',
    component: AddEditComponent,
    canActivate: [ AuthGuard ],
  },
  {
    path: 'courses/:id',
    component: AddEditComponent,
    canActivate: [ AuthGuard ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseRoutingModule {
}
