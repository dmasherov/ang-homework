// taken from https://www.techiediaries.com/fake-api-jwt-json-server/

const fs = require('fs')
const path = require('path')
const bodyParser = require('body-parser')
const jsonServer = require('json-server')
const jwt = require('jsonwebtoken')

const server = jsonServer.create()
const dbJsonPath = path.resolve(__dirname, 'db.json')
const router = jsonServer.router(dbJsonPath)

const users = [
  {
    "name": "admin",
    "email": "admin@admin.com",
    "password": "admin"
  },
  {
    "name": "admin",
    "email": "admin",
    "password": "admin"
  },
  {
    "name": "user",
    "email": "user@user.com",
    "password": "user"
  },
];

const SECRET_KEY = '123456789'
const expiresIn = '1h'

// Create a token from a payload 
function createToken(payload){
  return jwt.sign(payload, SECRET_KEY, {expiresIn})
}

// Verify the token 
function verifyToken(token){
  jwt.verify(token, SECRET_KEY);
}

// Check if the user exists in database
function isAuthenticated({email, password}){
  console.log(email, password);
  return users.findIndex(user => user.email === email && user.password === password) !== -1
}

server.use(jsonServer.defaults());
server.use(bodyParser.urlencoded({extended: true}))
server.use(bodyParser.json())

server.post('/auth/login', (req, res) => {
  if (!req.body) {
    const status = 400
    const message = 'Email and password should be provided'
    res.status(400).json({status, message})
    return
  }
  const {email, password} = req.body
  if (isAuthenticated({email, password}) === false) {
    const status = 401
    const message = 'Wrong e-mail or password'
    res.status(status).json({status, message})
    return
  }
  const token = createToken({email, password})
  res.status(200).json({token})
})

server.use(/^(?!\/auth).*$/,  (req, res, next) => {
  if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
    const status = 401
    const message = 'Bad authorization header'
    res.status(status).json({status, message})
    return
  }
  try {
     verifyToken(req.headers.authorization.split(' ')[1])
     next()
  } catch (err) {
    const status = 401
    const message = err.message;
    res.status(status).json({status, message})
  }
})

server.use(router)

server.listen(3000, () => {
  console.log('Auth API Server')
})
