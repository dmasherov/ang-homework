import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingOverlayService {

  private overlaySource = new BehaviorSubject(false);
  public overlaying$: Observable<boolean> = this.overlaySource.asObservable();

  constructor() { }

  public overlay() {
    this.overlaySource.next(true);
  }

  public clearOverlay() {
    this.overlaySource.next(false);
  }
}
