import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { SharedModule } from '../shared';

import { AddEditComponent } from './add-edit';
import { CourseComponent } from './course';
import { CourseRoutingModule } from './course-routing.module';
import { CoursesEffects } from './courses.effects';
import { courseReducer } from './courses.reducer';
import { CourseListComponent } from './list';
import { HighlightCourseDirective, HourMinuteFormatPipe } from './utils';

@NgModule({
  declarations: [
    AddEditComponent,
    CourseListComponent,
    CourseComponent,
    HighlightCourseDirective,
    HourMinuteFormatPipe,
  ],
  exports: [
    AddEditComponent,
    CourseListComponent,
    CourseComponent,
    HighlightCourseDirective,
    HourMinuteFormatPipe,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    CourseRoutingModule,
    StoreModule.forFeature('courses', courseReducer),
    EffectsModule.forFeature([CoursesEffects]),
  ]
})
export class CoursesModule { }
