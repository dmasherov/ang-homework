import { AbstractControl, ValidationErrors } from '@angular/forms';

export function integerValidator(control: AbstractControl): ValidationErrors {
  const val = control.value;
  const isInteger = Number.isInteger(val);
  return isInteger ? null : {nonInteger: true};
}
