import { formatDate } from '@angular/common';
import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateInputComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useValue: (dateInputControl: FormControl) => {
        const parsed = Date.parse(dateInputControl.value);
        return isNaN(parsed) ? { invalidDate: true } : null;
      },
      multi: true
    }
  ]
})
export class DateInputComponent implements ControlValueAccessor {

  constructor() { }

  public day: number;
  public month: number;
  public year: number;

  private onChange = (_: any) => {};
  private onTouched = () => {};

  public changeDate() {
    if (!!this.year && !!this.month && !!this.day) {
      const isoDate = `${this.year}-${this.month}-${this.day}`;
      this.onChange(isoDate);
    } else {
      this.onChange(null);
    }
  }

  public writeValue(isoDate: any) {
    if (!isoDate) {
      this.day = null;
      this.month = null;
      this.year = null;
    } else {
      const [year, month, day] = isoDate.split('-');
      this.day = day;
      this.month = month;
      this.year = year;
    }
  }

  public registerOnChange(fn: (_: any) => void) {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => void) {
    this.onTouched = fn;
  }
}
