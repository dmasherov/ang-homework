import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { CourseComponent } from '../course/course.component';
import { Course } from '../model';
import { HourMinuteFormatPipe } from '../utils/hour-minute-format.pipe';

describe('CourseComponent', () => {
  let component: CourseComponent;
  let fixture: ComponentFixture<CourseComponent>;

  const mockCourse: Course = {
    id: 1,
    title: 'title',
    creationDate: '2019-01-01',
    duration: 20,
    description: 'description',
    topRated: false,
    authors: [],
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseComponent, HourMinuteFormatPipe ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseComponent);
    component = fixture.componentInstance;
    component.course = mockCourse;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show course id and uppercased title in title element', () => {
    const titleDe = fixture.debugElement.query(By.css('.header > .title'));
    const titleEl = titleDe.nativeElement;

    expect(titleEl.textContent).toContain(`Video Course ${mockCourse.id}. ${mockCourse.title.toUpperCase()}`);
  });

  it('should emit course on click', () => {
    component.onDelete.subscribe(course => {
      expect(course).toBe(mockCourse);
    });
    const deleteButtonDe = fixture.debugElement.query(By.css('.action-buttons > .delete'));
    const deleteButtonEl = deleteButtonDe.nativeElement;

    deleteButtonEl.click();
  });
});

