import { Directive, ElementRef, Input, OnInit } from '@angular/core';

import { Course } from '../model';

@Directive({
  selector: '[appHighlightCourse]'
})
export class HighlightCourseDirective implements OnInit {

  @Input()
  public course: Course;

  constructor(private el: ElementRef) {
  }

  public ngOnInit() {
    const now = new Date();

    const creationDate = new Date(this.course.creationDate);
    if (now < creationDate) {
      this.el.nativeElement.style.border = '2px solid steelblue';
    } else {
      const nowMinus14 = new Date();
      nowMinus14.setDate(now.getDate() - 14);
      if (creationDate >= nowMinus14) {
        this.el.nativeElement.style.border = '2px solid lime';
      }
    }
  }
}
