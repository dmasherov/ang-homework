import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { OrderByPipe } from '../../shared';
import { CourseComponent } from '../course/course.component';
import { Course } from '../model';
import { HourMinuteFormatPipe } from '../utils/hour-minute-format.pipe';

import { CourseListComponent } from './course-list.component';

describe('CourseListComponent', () => {
  let component: CourseListComponent;
  let fixture: ComponentFixture<CourseListComponent>;

  const createMockCourse = (id, title) => ({
      id, title, creationDate: '2019-01-01', duration: 20, description: 'description', topRated: false
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseListComponent, CourseComponent, HourMinuteFormatPipe, OrderByPipe ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should show and allow click search button', () => {
    const searchButtonDe = fixture.debugElement.query(By.css('.search-button'));
    const searchButtonEl = searchButtonDe.nativeElement;

    expect(searchButtonEl.textContent).toBe('Search');
  });

  it('show multiple courses', () => {
    component.courses = [createMockCourse(1, '1'), createMockCourse(2, '2')];
    fixture.detectChanges();

    const courseEls = fixture.debugElement.queryAll(By.css('app-course'));

    expect(courseEls.length).toBe(2);
  });

  it('allow to click delete course', () => {
    spyOn(component, 'delete').and.callThrough();
    spyOn(window, 'confirm').and.returnValue(false);

    component.courses = [createMockCourse(1, '1'), createMockCourse(2, '2')];
    fixture.detectChanges();

    const buttonDe = fixture.debugElement.query(By.css('.delete'));
    buttonDe.nativeElement.click();

    expect(component.delete).toHaveBeenCalledWith(component.courses[0].id);
  });

  it('allow to click loadMore', () => {
    spyOn(component, 'loadMore').and.callThrough();

    const buttonDe = fixture.debugElement.query(By.css('.load-more-button'));
    buttonDe.nativeElement.click();

    component.loadMore();
    expect(component.loadMore).toHaveBeenCalled();
  });

  it('allow to click search', () => {
    spyOn(component, 'search').and.callThrough();

    const buttonDe = fixture.debugElement.query(By.css('.search-button'));
    buttonDe.nativeElement.click();

    expect(component.search).toHaveBeenCalled();
  });
});
