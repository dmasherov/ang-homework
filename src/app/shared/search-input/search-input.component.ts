import { Component, EventEmitter, Output } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.css']
})
export class SearchInputComponent implements ControlValueAccessor {

  public searchTerm: string = null;

  @Output()
  public onSearch = new EventEmitter<string>();

  public onChange = (_) => {};
  public onTouched = () => {};

  constructor() { }

  public search() {
    this.onChange(this.searchTerm);
    if (this.searchTerm) {
      this.onSearch.emit(this.searchTerm);
    }
  }

  public writeValue(val) {
    this.searchTerm = val;
  }

  public registerOnChange(fn) {
    this.onChange = fn;
  }

  public registerOnTouched(fn) {
    this.onTouched = fn;
  }

}
