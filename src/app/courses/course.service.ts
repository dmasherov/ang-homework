import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { LoadingOverlayService } from '../shared';

import { Course, Person } from './model';

const COURSE_API = '/api/courses';
const PEOPLE_API = '/api/people';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private loadingOverlayService: LoadingOverlayService, private http: HttpClient) { }

  public getCoursePage(page: number = 0, searchTerm: string = '', limit: number = 5): Observable<Course[]> {
    const pageParams: any = { _page: page + '', _limit: limit + '' };
    const params = searchTerm ? {...pageParams, q: searchTerm } : pageParams;
    this.loadingOverlayService.overlay();
    return this.http.get<Course[]>(COURSE_API, { params }).pipe(
      tap(() => this.loadingOverlayService.clearOverlay())
    );
  }

  public searchCourses(searchText: string): Observable<Course[]> {
    const params = { q: searchText };
    this.loadingOverlayService.overlay();
    return this.http.get<Course[]>(COURSE_API, { params }).pipe(
      tap(() => this.loadingOverlayService.clearOverlay())
    );
  }

  public createCourse(course: Course): Observable<Course> {
    this.loadingOverlayService.overlay();
    return this.http.post<Course>(COURSE_API, course).pipe(
      tap(() => this.loadingOverlayService.clearOverlay())
    );
  }

  public getCourseById(id: number): Observable<Course> {
    this.loadingOverlayService.overlay();
    return this.http.get<Course>(`${COURSE_API}/${id}`).pipe(
      tap(() => this.loadingOverlayService.clearOverlay())
    );
  }

  public updateCourse(id: number, course: Course): Observable<Course> {
    this.loadingOverlayService.overlay();
    return this.http.put<Course>(`${COURSE_API}/${id}`, course).pipe(
      tap(() => this.loadingOverlayService.clearOverlay())
    );
  }

  public removeCourse(id: number): Observable<Course> {
    this.loadingOverlayService.overlay();
    return this.http.delete<Course>(`${COURSE_API}/${id}`).pipe(
      tap(() => this.loadingOverlayService.clearOverlay())
    );
  }

  public searchAuthors(searchTerm: string): Observable<Person[]> {
    const params = { q: searchTerm, _limit: '5' };
    return this.http.get<Person[]>(PEOPLE_API, { params });
  }
}
