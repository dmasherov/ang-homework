import { createReducer, on } from '@ngrx/store';

import * as AuthActions from './auth.actions';
import { User } from './user';

export interface AuthState  {
  user: User;
  errorMessage: string;
}

const initialState = {
  user: null,
  errorMessage: null
};

export const authReducer = createReducer(
  initialState,
  on(AuthActions.loginDone, (state, { user }) => ({...state, user })),
  on(AuthActions.loginFail, (state, { message }) => ({...state, errorMessage: message })),
  on(AuthActions.logout, state => ({...state, user: null, errorMessage: null })),
);

export const clearStateOnLogoutReducer = (reducer) => (state, action) => {
  if (action.type === AuthActions.logout.type) {
    return reducer(undefined, action);
  }
  return reducer(state, action);
};
