import { createReducer, on } from '@ngrx/store';

import * as CourseActions from './courses.actions';
import { Course, Person } from './model';

export interface CoursesState {
  items: Course[];
  searchTerm: string;
  currentPage: number;
  canLoadMore: boolean;
  currentItem: Course;
  suggestedAuthors: Person[];
}

export const initialState: CoursesState = {
  items: [],
  searchTerm: null,
  currentPage: 0,
  canLoadMore: true,
  currentItem: null,
  suggestedAuthors: []
};

export const courseReducer = createReducer(
  initialState,
  on(CourseActions.initialize, state => {
    return {...state, items: [], currentPage: 1};
  }),
  on(CourseActions.loadMore, state => {
    if (state.canLoadMore) {
      return {...state, currentPage: state.currentPage + 1};
    }
    return state;
  }),
  on(CourseActions.loadSuccess, (state, { items: loadedItems }) => {
    const items = state.items.concat(loadedItems);
    const canLoadMore = loadedItems.length > 0;
    return { ...state, items, canLoadMore };
  }),
  on(CourseActions.search, ( state, { searchTerm } ) => ({ ...initialState, searchTerm })),
  on(CourseActions.itemLoaded, (state, {item}) => ({...state, currentItem: item})),
  on(CourseActions.searchAuthorsResult, (state, {personList}) => ({...state, suggestedAuthors: personList})),
);
