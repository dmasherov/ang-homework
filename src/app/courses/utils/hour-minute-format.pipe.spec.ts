import { HourMinuteFormatPipe } from './hour-minute-format.pipe';

describe('HourMinuteFormatPipe', () => {

  const mockTranslateService: any = {
    get: (key: string) => {
      if (key == 'time.short-hour') return 'h';
      if (key == 'time.short-minute') return 'min';
    }
  }
  const pipe = new HourMinuteFormatPipe(mockTranslateService);

  it('display hours and minutes when duration is more than 60', () => {
    const formatedDuration = pipe.transform(125);
    expect(formatedDuration).toEqual('2h 5min');
  });

  it('display only minutes when duration is less than 60', () => {
    const formatedDuration = pipe.transform(35);
    expect(formatedDuration).toEqual('35min');
  });
});
