import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { LoadingOverlayService } from './loading-overlay.service';

@Component({
  selector: 'app-loading-block',
  templateUrl: './loading-block.component.html',
  styleUrls: ['./loading-block.component.css']
})
export class LoadingBlockComponent {

  public overlaying$: Observable<boolean>;

  constructor(loadingOverlayService: LoadingOverlayService) {
    this.overlaying$ = loadingOverlayService.overlaying$;
  }

}
