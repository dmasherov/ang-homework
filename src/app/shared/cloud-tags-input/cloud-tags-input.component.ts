import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'cloud-tags-input',
  templateUrl: './cloud-tags-input.component.html',
  styleUrls: ['./cloud-tags-input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CloudTagsInputComponent),
      multi: true
    }
  ],
})
export class CloudTagsInputComponent implements ControlValueAccessor {

  constructor() { }

  @Input()
  public displayKey: string = null;

  @Output()
  public onSuggest = new EventEmitter<string>();

  public itemList: any[];
  public newItemValue: string;

  @Input()
  public suggestions: any[] = [];

  private onChange = (_: any) => {};
  private onTouched = () => {};

  public typingNewItem(event) {
    if (this.newItemValue) {
      this.onSuggest.emit(this.newItemValue);
    }
  }

  public selectSuggestion(suggestion: any) {
    this.itemList = [...this.itemList, suggestion];
    this.suggestions = [];
    this.newItemValue = null;
    this.onChange(this.itemList);
  }

  public remove(index: number) {
    this.itemList = this.itemList.filter((_, i) => i !== index);
    this.onChange(this.itemList);
  }

  public writeValue(itemList: any[]) {
    this.itemList = itemList;
  }

  public registerOnChange(fn: (_: any) => void) {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => void) {
    this.onTouched = fn;
  }
}
